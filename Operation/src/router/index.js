import Vue from 'vue'
import Router from 'vue-router'


//入口界面
// import Entrance from '../pages/entrance.vue'
// import WantAccess from '../pages/wantAccess.vue'
const Entrance =  r => require.ensure([], () => r(require('../pages/entrance')), 'group-a')
const WantAccess =  r => require.ensure([], () => r(require('../pages/wantAccess')), 'group-b')

//主界面
// import Main from '../pages/main.vue'
const Main =  r => require.ensure([], () => r(require('../pages/main')), 'group-c')
//管理员
// import addAdmin from '../pages/addAdmin.vue'
// import addToolPermission from '../pages/addToolPermission.vue'
// import addDevelop from '../pages/addDevelop.vue'
// import addSeller from '../pages/addSeller.vue'
const addAdmin =  r => require.ensure([], () => r(require('../pages/addAdmin')), 'group-d')
const addToolPermission =  r => require.ensure([], () => r(require('../pages/addToolPermission')), 'group-e')
const addDevelop =  r => require.ensure([], () => r(require('../pages/addDevelop')), 'group-f')
const addSeller =  r => require.ensure([], () => r(require('../pages/addSeller')), 'group-g')
// 查询
// import Querydevice  from '../pages/querydevice.vue'
// import QueryImeiList  from '../pages/queryImeiList.vue'
// import QueryServer  from '../pages/queryserver.vue'
// import QueryReloadCnt  from '../pages/queryReloadCnt.vue'
const Querydevice =  r => require.ensure([], () => r(require('../pages/querydevice')), 'group-h')
const QueryImeiList =  r => require.ensure([], () => r(require('../pages/queryImeiList')), 'group-i')
const QueryServer =  r => require.ensure([], () => r(require('../pages/queryServer')), 'group-j')
const QueryReloadCnt =  r => require.ensure([], () => r(require('../pages/queryReloadCnt')), 'group-jk')
// 报警
// import queryTel from '../pages/queryTel.vue'
// import callTel from '../pages/callTel.vue'
// import delTel from '../pages/delTel.vue'
// import modifyTel from '../pages/modifyTel.vue'
const queryTel =  r => require.ensure([], () => r(require('../pages/queryTel')), 'group-k')
const callTel =  r => require.ensure([], () => r(require('../pages/callTel')), 'group-l')
const delTel =  r => require.ensure([], () => r(require('../pages/delTel')), 'group-m')
const modifyTel =  r => require.ensure([], () => r(require('../pages/modifyTel')), 'group-n')

// 设置
// import Bind from '../pages/bind.vue'
// import Set    from '../pages/set.vue'
// import changeServer from '../pages/changeServer.vue'
// import motor from '../pages/motor.vue'
// import upgrade from '../pages/upgrade.vue'
const Bind =  r => require.ensure([], () => r(require('../pages/bind')), 'group-o')
const Set =  r => require.ensure([], () => r(require('../pages/set')), 'group-p')
const changeServer =  r => require.ensure([], () => r(require('../pages/changeServer')), 'group-q')
const motor =  r => require.ensure([], () => r(require('../pages/motor')), 'group-r')
const upgrade =  r => require.ensure([], () => r(require('../pages/upgrade')), 'group-s')

Vue.use(Router)

export default new Router({
      routes: [
        // 入口
        {
           path:"/",
           name:"Entrance",
           component:Entrance,
        },
        // 没有权限
        {
           path:"/WantAccess",
           name:"WantAccess",
           component:WantAccess,
        },
        //主界面
        {
          path:"/Main",
          name:"Main",
          component:Main,
          children:[
              // 添加管理员
              {
                 path:"/addAdmin",
                 name:"addAdmin",
                 component:addAdmin,
              },
              {
                 path:"/addDevelop",
                 name:"addDevelop",
                 component:addDevelop,
              },
              {
                 path:"/addSeller",
                 name:"addSeller",
                 component:addSeller,
              },
              {
                path:"/addToolPermission",
                name:"addToolPermission",
                component:addToolPermission,
             },
              // 查询
              {
                path: '/Querydevice',
                name:'Querydevice',
                component: Querydevice
              },
              {
                path: '/QueryImeiList',
                name:'QueryImeiList',
                component: QueryImeiList
              },
              {
                path: '/QueryReloadCnt',
                name:'QueryReloadCnt',
                component: QueryReloadCnt
              },
              {
                path: '/QueryServer',
                name:'QueryServer',
                component: QueryServer
              },
              // 报警
              {
                path: '/queryTel',
                name:'queryTel',
                component: queryTel
              },
              {
                path: '/callTel',
                name:'callTel',
                component: callTel
              },
              {
                path: '/delTel',
                name:'delTel',
                component: delTel
              },
              {
                path: '/modifyTel',
                name:'modifyTel',
                component: modifyTel
              },
              // 绑定
              {
                path: '/Bind',
                name:'Bind',
                component: Bind
              },
              // 设置
              {
                path: '/Set',
                name:'Set',
                component: Set
              },
              // 更改设备服务器
              {
                path: '/changeServer',
                name:'changeServer',
                component: changeServer
              },
              // 注册为摩托车版本
              {
                path: '/motor',
                name:'motor',
                component: motor
              },
              // 升级
              {
                path: '/upgrade',
                name:'upgrade',
                component: upgrade
              }
         ]
       },
     ]
})
