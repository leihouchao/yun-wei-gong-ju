// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import $ from 'jquery'
import iView from 'iView'
import VueResource from 'vue-resource';
import 'lib-flexible'
import 'iView/dist/styles/iview.css'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
Vue.use(ElementUI)

Vue.use(VueResource);
Vue.use(iView);
Vue.config.productionTip = false

// 初始化leanCloud
var APP_ID = '5wk8ccseci7lnss55xfxdgj9xn77hxg3rppsu16o83fydjjn';//'5wk8ccseci7lnss55xfxdgj9xn77hxg3rppsu16o83fydjjn';
var APP_KEY = 'yovqy5zy16og43zwew8i6qmtkp2y6r9b18zerha0fqi5dqsw';//'yovqy5zy16og43zwew8i6qmtkp2y6r9b18zerha0fqi5dqsw';
AV.init({
    appId: APP_ID,
    appKey: APP_KEY
  });

function getUserInfo(){
        var APPID = "dingoadwptluimajpymsch";
        var REDIRECT_URL = "http://www.xiaoantech.com/operation";
        var checkUrl = "https://oapi.dingtalk.com/connect/qrconnect?appid="
                       + APPID + "&response_type=code&scope=snsapi_login&state=STATE&redirect_uri="
                       + REDIRECT_URL;
        var param = window.location.search;
        if(param == ""){
          // 第一次进入页面，跳转到钉钉验证页面
            window.location.href = checkUrl;
        }
        else{
           var code = param.substr(6,32);
        //    console.log("code: ",code );

           var APPSECRET = "3ax4bBdemEj8Sduj4nfnNC4MTwS6JJKicz8f-OCBkS4zKl7788BtQ6k7T3Yo1QuD";
           var URL_accessToken = "http://www.xiaoantech.com/sns/gettoken?appid=" + APPID
                 + "&appsecret=" + APPSECRET;

          $.ajax({
           url : URL_accessToken,
           type : "GET",
           dataType : "json", // 返回的数据类型，设置为JSONP方式
           success: function(res){
             if(res.errcode === 0){
                 var access_token = res.access_token;
                //  console.log("access_token: ",access_token);
                 // 2，获取用户的永久码
                 var URL_persistentCode = "http://www.xiaoantech.com/sns/get_persistent_code?access_token=" + access_token;
                 var data_persistentCode = "{\"tmp_auth_code\":\"" + code + "\"}";
                 $.ajax({
                     url:URL_persistentCode,
                     dataType: 'json',
                     contentType: "application/json; charset=utf-8",
                     type: 'POST',
                     data: data_persistentCode,
                     success: function(res){
                       if(res.errcode == 0){
                            //3,获取用户sns_token
                            // console.log("永久code: ",res.persistent_code);
                            var persistentCode = res.persistent_code;
                            var URL_snsToken = "http://www.xiaoantech.com/sns/get_sns_token?access_token=" + access_token;
                            var data_snsToken =  "{	\"openid\":\"" + res.openid + "\",\"persistent_code\": \"" + res.persistent_code + "\"}";
                            $.ajax({
                                url:URL_snsToken,
                                dataType: 'json',
                                contentType: "application/json; charset=utf-8",
                                type: 'POST',
                                data:data_snsToken,
                                success:function(res){
                                    //4,获取用户信息
                                    if(res.errcode == 0){
                                        //  console.log("sns_token: ",res.sns_token);
                                         var URL_userInfo = "http://www.xiaoantech.com/sns/getuserinfo?sns_token=" + res.sns_token;
                                         $.ajax({
                                              url:URL_userInfo,
                                              dataType:'json',
                                              type:'GET',
                                              success:function(res){
                                                  //  alert("你好! " + res.user_info.nick);
                                                    // console.log("你好! " + res.user_info.nick);
                                                    localStorage.setItem("ACCESS_USER_NAME",res.user_info.nick);

                                              },
                                              error:function(){
                                                  alert("4,获取用户信息失败")
                                              }
                                    });
                                  }
                                  else {
                                     console.log("获取sns_token出错: ",res);
                                  }
                                },
                                error:function(){
                                    alert("3,读取sns_token失败");
                                }
                              });
                       }
                       else{
                          console.log("获取用户永久code报错：",res);
                       }
                     },
                     error:function(){
                         alert("2,获取用户永久码失败");
                     }
                   });
              }
           },
           error: function(){
                alert("1,读取access_token出错");
           }
          });
       }
}
function checkLevel(){
     if(localStorage.hasOwnProperty('ACCESS_USER_NAME') ){
     }
     else{
         getUserInfo();
        //  while(!localStorage.hasOwnProperty('ACCESS_USER_NAME') );
     }
}
checkLevel();

new Vue({
     el: '#app',
     router,
     template: '<App/>',
     components: {App}
});
